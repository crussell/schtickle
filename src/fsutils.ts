/* This Software is subject to the terms of The MIT License. */
/* See other notices for parts governed by additional terms. */

var fs = require("fs");

export function ensureDirectories(path: string) {
  let directories = path.split("/");
  let current = "";
  // NB: n
  for (let i = 0, n = directories.length - 1; i < n; ++i) {
    current += directories[i] + "/";
    if (!directoryExists(current)) {
      fs.mkdirSync(current);
    }
  }
}

export function directoryExists(path: string): boolean {
  return exists(path, true);
}

export function fileExists(path: string): boolean {
  return exists(path, false);
}

function exists(path: string, asDirectory: boolean): boolean {
  try {
    var stat = fs.statSync(path);
  }
  catch (ex) {
    return false;
  }

  // If it exists but as a different type, let the Error escape to the caller.
  return (asDirectory) ?
    stat.isDirectory() :
    stat.isFile();
}

export function copyFileSync(inpath: string, outpath: string) {
  ensureDirectories(outpath);
  let buffer = new Buffer(65536);
  let infd = fs.openSync(inpath, "r");
  let outfd = fs.openSync(outpath, "w");

  let bytesRead, pos = 0;
  do {
    bytesRead = fs.readSync(infd, buffer, 0, 65536, pos);
    fs.writeSync(outfd, buffer, 0, bytesRead);
    pos += bytesRead;
  } while (bytesRead > 0);

  fs.closeSync(infd);
  fs.closeSync(outfd);
}

export function getExtension(fileName: string): string {
  let extension = fileName.substring(fileName.lastIndexOf(".") + 1);
  return (extension.length != fileName.length) ?
    extension :
    null;
}
