/* This Software is subject to the terms of The MIT License. */
/* See other notices for parts governed by additional terms. */

var fs = require("fs");
var DateFormat = require("dateformat");
var Marked = require("marked");
var Mold = require("mold-template");
var Rimraf = require("rimraf");
var Yaml = require("js-yaml");

import { readContents, FrontMatter } from "drmime";

import * as FSUtils from "./fsutils";
import * as CommandLineParser from "./commandLineParser";

interface Config {
  destination: string;
  layouts: string;
  includes: string;
  posts: string;
  defaultLayout: string;
  postLayout: string;
  postLink: string;
  inputFile: string;
}

interface DocParts {
  frontMatter: FrontMatter;
  mainText: { toString(): string }
}

interface Doc {
  isPost?: boolean;
  url: string;
  slug: string;
  bare: boolean;
  tags: string[];
  content: string;
  layout: string;
}

interface Post extends Doc {
  date: Date;
  // isLink: boolean;
}

interface MoldContext {
  site: {
    posts: Post[];
    tags: Map<string, Doc[]>;
    config: Config;
  };
  dateFormat: any;
}

interface BakedTemplate {
  (arg?: any): string;
}

interface Layout {
  template: BakedTemplate;
  parentName?: string;
  execute(doc: Doc): string;
}

class FreshLayout implements Layout {
  parentName: string = null;

  constructor(
    public template,
    public context: MoldContext,
    public config: Config
  ) {};

  execute(doc: Doc)
    : string
  {
    let text = this.template(doc);
    if (this.parentName) {
      let parent = LayoutKeeper.get(this.parentName, this.context, this.config);
      let dummyDoc = Object.create(doc, { content: { value: text } } );
      text = parent.execute(dummyDoc);
    }

    return text;
  }

  static fromParts(parts: DocParts, context: MoldContext, config: Config)
    : Layout
  {
    let { mainText, frontMatter } = parts;
    let template = <BakedTemplate>Mold.bake(String(mainText), context);
    let result = new FreshLayout(template, context, config);
    if (frontMatter && "layout" in frontMatter) {
      result.parentName = frontMatter["layout"];
    }

    return result;
  }
}

class LayoutKeeper {
  static cache = new Map<string, Layout>();

  static get(name: string, context: MoldContext, config: Config)
    : Layout
  {
    let fileName = (!FSUtils.getExtension(name)) ?
      name + ".html" :
      name;
    if (this.cache.has(fileName)) return this.cache.get(fileName);

    let layoutText: string;
    try {
      layoutText = fs.readFileSync(config.layouts + fileName, "utf8");
    }
    catch (ex) {
      if (name != config.defaultLayout) throw ex;
      if (config.defaultLayout != ConfigReader.defaults.defaultLayout) throw ex;
      layoutText =
        "<!doctype html>\n" +
        "<head><title><?text $arg.title?></title></head>\n" +
        "<body><?html $arg.content?></body>";
    }

    let parts = readContents(layoutText);
    let result = FreshLayout.fromParts(parts, context, config);

    this.cache.set(fileName, result);
    return result;
  }
}

class ConfigReader {
  // This isn't an overridable in `defaults` because there's a chicken-and-
  // egg problem; we can't use the config file to specify its own path.
  static configFileName = "_config.yml";

  static defaults = {
    destination: "_site/", // NB: Not a "true" default; may end up as null...
    layouts: "_layouts/",
    includes: "_includes/",
    posts: "_posts/",
    defaultLayout: "default",
    postLayout: "post.html",
    postLink: "${name}.html",
    inputFile: null
  };

  static parse(configFileName: string, cliOptions: any[], inputFileName: string)
    : Config
  {
    let config = (FSUtils.fileExists(configFileName)) ?
      Yaml.load(fs.readFileSync(configFileName, "utf8")) :
      {};

    if (inputFileName) {
      config.inputFile = inputFileName;

      // ... because in single-file mode, we want to write to stdout instead of
      // writing out the typical directory structure to disk (unless a
      // destination was explicitly provided, in which case we'll comply).  By
      // nulling this out, we avoid absorbing the value from the defaults in the
      // loop below.
      if (!("destination" in config)) config.destination = null;
    }

    for (let key in this.defaults) {
      if (!config.hasOwnProperty(key)) config[key] = this.defaults[key];
    }

    return config;
  }
}

function parsePost(fileText: string, fileName: string, config: Config)
  : Post
{
  // TODO Allow posts that give a date in the YAML, rather than requiring one
  // in the file name.
  // TODO Support .link files.
  let fields = fileName.match(
    /^(\d{4})-(\d\d?)-(\d\d?)-(.+)\.(md|markdown|html)$/
  );

  if (!fields) return null;
  let [ year, monthNumber, day, name, extension ] = fields.slice(1);

  let post = <Post>parseDoc(fileText, fileName, config.postLayout);
  if (post) {
    post.isPost = true;
    post.slug = name;
    post.date = new Date(
      parseInt(year, 10), parseInt(monthNumber, 10) - 1, parseInt(day, 10)
    );
  }

  return post;
}

/**
 * @param fileText
 *        The contents of the file being processed.
 * @param fileName
 *        The name of the file being processed.  (Does not include the path of
 *        the parent directory.)
 * @param layout
 *        NB: This is only the default layout.  It will not be used if the
 *        frontmatter explicitly names a layout to use instead.
 * @return
 *        A `Doc` whose properties are filled in by the corresponding
 *        frontmatter, or null if we failed in our attempts to parse it (e.g.,
 *        binary files like images, or any other foreign file type).
 */
function parseDoc(fileText: string, fileName: string, layout: string)
  : Doc
{
  let extension = FSUtils.getExtension(fileName);

  let structure = readContents(fileText);
  let doc = <Doc>(structure.frontMatter || {});
  // NB: Markdown is allowed to omit the frontmatter section and will still
  // recognized correctly.
  if (extension == "md" || extension == "markdown") {
    doc.content = Marked(String(structure.mainText));
  }
  else if (structure.frontMatter) {
    // Non-markdown files may contain useful frontmatter; e.g., Jekyll supports
    // YAML+HTML.  See https://github.com/marijnh/heckle/issues/6
    doc.content = String(structure.mainText);
  }
  else {
    // Non-markdown files without frontmatter cannot be handled here.
    return null;
  }

  doc.bare = !extension;
  doc.slug = (extension) ?
    fileName.substring(0, fileName.length - extension.length - 1) :
    fileName;

  if (!("layout" in doc)) doc.layout = layout;

  let tags = [];
  if (structure.frontMatter && "tags" in structure.frontMatter) {
    tags = (typeof(structure.frontMatter['tags']) == "string") ?
      structure.frontMatter['tags'].split(/\s+/) :
      structure.frontMatter['tags'];
  }
  doc.tags = tags;

  return doc;
}

function getURL(doc: Doc, config: Config)
  : string
{
  // TODO Use `config.postLink`.  We want to allow allow ES6-like template
  // strings, like heckle is shooting for.
  let url = doc.slug;
  if (!doc.bare) url += ".html";

  if (doc.isPost) {
    url = DateFormat((<Post>doc).date, "yyyy/mm/dd") + "/" + url;
  }

  return url;
}

function collateTags(docs: Doc[])
  : Map<string, Doc[]>
{
  let result = new Map<string, Doc[]>();
  for (let i = 0, n = docs.length; i < n; ++i) {
    for (let j = 0, m = docs[i].tags.length; j < m; ++j) {
      let tag = docs[i].tags[j];
      if (!result.has(tag)) result.set(tag, []);
      result.get(tag).push(docs[i]);
    }
  }

  return result;
}

function prepareIncludes(context: MoldContext, config: Config) {
  if (!FSUtils.directoryExists(config.includes)) return;
  let files = fs.readdirSync(config.includes);
  for (let i = 0, n = files.length; i < n; ++i) {
    let include = Mold.bake(
      fs.readFileSync(config.includes + files[i], "utf8")
    );
    let name = files[i].substring(0, files[i].lastIndexOf(".")) || files[i];
    Mold.define(name, include, context);
  }
}

function generate(config: Config) {
  let context = {
    dateFormat: DateFormat,
    site: { config: config, posts: [], tags: null }
  };
  prepareIncludes(context, config);

  if (config.destination && FSUtils.directoryExists(config.destination)) {
    Rimraf.sync(config.destination);
  }

  // First: are we snarfing in a full-fledged, quasi-jekyll site?
  if (config.inputFile) {
    // Nope; single-file mode.
    let inputText = fs.readFileSync(config.inputFile, "utf8");
    // NB: `config.inputFile` may be a path to a file outside the current
    // directory.  `parseDoc` expects a leaf name, not a qualified path.
    let basename = config.inputFile.split("/").pop();
    let doc = parseDoc(inputText, basename, config.defaultLayout);
    let outputText = prepareDocument(doc, context, config);
    if (config.destination) {
      let fileName = getFileName(doc, config);
      console.log("writing " + fileName);
      FSUtils.ensureDirectories(fileName);
      fs.writeFileSync(fileName, outputText, "utf8");
    }
    else {
      process.stdout.write(outputText);
    }

    // We're done.
    return;
  }

  // We're working with a site that has a jekyll-style directory structure.
  let docs: Doc[] = [];
  let fileNames = (FSUtils.directoryExists(config.posts)) ?
    fs.readdirSync(config.posts) :
    [];
  for (let i = 0, n = fileNames.length; i < n; ++i) {
    if (fileNames[i].charAt(0) == ".") continue;
    let path = config.posts + fileNames[i];
    console.log("processing post " + path);
    let fileText = fs.readFileSync(path, "utf8");
    let post = parsePost(fileText, fileNames[i], config);
    if (!post) console.log("  ... failed.");
    if (post.tags.indexOf("hidden") < 0) context.site.posts.push(post);
    docs.push(post);
  }

  context.site.posts.sort(function (a: Post, b: Post) {
    return Number(b.date) - Number(a.date);
  });

  console.log("writing " + config.destination);

  docs.push(...walkDir(".", config));

  for (let i = 0, n = docs.length; i < n; ++i) {
    let fileText = prepareDocument(docs[i], context, config);
    let fileName = getFileName(docs[i], config);
    FSUtils.ensureDirectories(fileName);
    fs.writeFileSync(fileName, fileText, "utf8");
  }
}

function prepareDocument(doc: Doc, context: MoldContext, config: Config)
  : string
{
  if (!doc.url) doc.url = getURL(doc, config);
  let parts = {
    frontMatter: doc,
    mainText: { toString() { return doc.content; } }
  };
  let layout = FreshLayout.fromParts(parts, context, config);
  return layout.execute(doc);
}

function getFileName(doc: Doc, config: Config)
  : string
{
  // For most single-file mode invocations, `config.destination` will be null.
  return (config.destination) ?
    config.destination + doc.url :
    doc.url;
}

function walkDir(directory: string, config: Config)
  : Doc[]
{
  let result: Doc[] = [];
  let files = fs.readdirSync(directory);
  for (let i = 0, n = files.length; i < n; ++i) {
    let path = directory + "/" + files[i];
    if (files[i].charAt(0) == ".") continue;
    if (files[i].charAt(0) == "_") {
      // Make sure we don't print skipping message for _posts, because it
      // would be a lie; we're skipping it *now*, but we just processed it.
      if (files[i] != config.posts) console.log("skipping " + path);
      continue;
    }

    if (fs.statSync(path).isDirectory()) {
      result.push(...walkDir(path, config));
      continue;
    }

    let fileText = fs.readFileSync(path, "utf8");
    let doc = parseDoc(fileText, files[i], config.defaultLayout);
    if (doc) {
      console.log("processing document " + path);
      result.push(doc);
    }
    else {
      console.log("copying " + path);
      FSUtils.copyFileSync(path, config.destination + path);
    }
  }

  return result;
}

export function main(argc: number, argv: string[])
{
  // TODO:
  // - Take -C/--directory args
  // - Take -t/--destination args
  let { options, fileNames } =
    CommandLineParser.parseCommandLine(argv.slice(2), []);

  if (options.length || fileNames.length > 1) {
    console.log("Usage: schtickle [FILE]");
  }

  let inputFileName = (fileNames.length) ?
    fileNames[0] :
    null;

  let config =
    ConfigReader.parse(ConfigReader.configFileName, options, inputFileName);

  generate(config);
}
