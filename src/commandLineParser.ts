/* This Software is subject to the terms of The MIT License. */
/* See other notices for parts governed by additional terms. */

/**
 * @fileoverview
 * Contains useful parts that come from the TypeScript compiler to handle
 * command-line args.
 *
 * NB: Microsoft's TypeScript compiler is under Apache 2.0, so be aware that
 * this is encumbered by those terms, at least in part.
 * 
 * @see OTHER NOTICES.txt
 */
export interface CommandLineOptionBase {
  name: string;
  type: "string" | "number" | "boolean" | Map<string, number>;
  shortName?: string;
}

export interface CommandLineOptionOfPrimitiveType extends CommandLineOptionBase {
  type: "string" | "number" | "boolean";
}

export interface CommandLineOptionOfCustomType extends CommandLineOptionBase {
  type: Map<string, number>;
}

export type CommandLineOption =
  CommandLineOptionOfCustomType | CommandLineOptionOfPrimitiveType;

interface OptionNameMap {
  optionNameMap: Map<string, CommandLineOption>;
  shortOptionNames: Map<string, string>;
}

let optionNameMapCache: OptionNameMap;
function getOptionNameMap(spec: CommandLineOption[])
  : OptionNameMap
{
  if (optionNameMapCache) {
    return optionNameMapCache;
  }

  const optionNameMap = new Map<string, CommandLineOption>();
  const shortOptionNames = new Map<string, string>();
  spec.forEach(option => {
    optionNameMap.set(option.name.toLowerCase(), option);
    if (option.shortName) {
      shortOptionNames.set(option.shortName, option.name);
    }
  });

  optionNameMapCache = { optionNameMap, shortOptionNames };
  return optionNameMapCache;
}

export interface ParsedCommandLine {
  options: any;
  fileNames: string[];
}

export function parseCommandLine(args: string[], spec: CommandLineOption[])
  : ParsedCommandLine
{
  const options = {};
  const fileNames: string[] = [];
  const { optionNameMap, shortOptionNames } = getOptionNameMap(spec);

  let i = 0;
  while (i < args.length) {
    let s = args[i];
    i++;
    if (s.charAt(0) == "-") {
      s = s.slice(s.charAt(1) == "-" ? 2 : 1).toLowerCase();

      // Try to translate short option names to their full equivalents.
      if (shortOptionNames.has(s)) {
        s = shortOptionNames.get(s);
      }

      if (optionNameMap.has(s)) {
        const opt = optionNameMap.get(s);

        if (!args[i] && opt.type !== "boolean") {
          throw new Error("Option " + opt.name + "expects an argument");
        }

        switch (opt.type) {
          case "number":
            options[opt.name] = parseInt(args[i]);
            i++;
          break;
          case "boolean":
            options[opt.name] = true;
          break;
          case "string":
            options[opt.name] = args[i] || "";
            i++;
          break;
          // If not a primitive, the possible types are specified in what is
          // effectively a map of options.
          default:
            let map = <Map<string, number>>opt.type;
            let key = (args[i] || "").toLowerCase();
            i++;
            if (map.has(key)) {
              options[opt.name] = map[key];
            }
            else {
              throw new Error(
                "Unknown value " + key + (args[i] || "") + " for " + opt.name
              );
            }
        }
      }
      else {
        throw new Error("Unknown option " + s);
      }
    }
    else {
      fileNames.push(s);
    }
  }

  return { options, fileNames };
}
