OUTFILE=./dist/schtickle.js
DEFFILE=./dist/schtickle.d.ts
MAIN_SRC=./src/*
TSC=./node_modules/typescript/bin/tsc
NPM=npm

LINK=$(HOME)/bin/schtickle

# Default make target; runs when invoking a simple `make`
all: package.json $(OUTFILE) $(DEFFILE)

$(OUTFILE) $(DEFFILE): node_modules $(MAIN_SRC) $(TSC)
	# tsc builds by implicitly reading options from tsconfig.json
	# This is what it looks like:
	@cat ./tsconfig.json
	# Let's build with it
	$(TSC) --declaration

$(TSC): node_modules
node_modules:
	# ... dependencies are specified in package.json
	$(NPM) install

rebuild: clean all

clean:
	# (I'm wary of publishing a build recipe that uses `rm -rf` anywhere.)
	mkdir -p ./dist # Just in case it didn't already exist
	rm -f ./dist/*
	rmdir ./dist
	rm -f drmime-*.tgz

# NB: You might think our second and third uses of `-I xx` are superfluous
# here, unlike the line above.  This is actually a trick to force GNU make
# to kind of behave like `xargs -r`, because `-r` is a non-portable GNU
# extension.
squeakyclean: clean
	mkdir -p ./node_modules/@types # prevents ls and rmdir from ever failing
	ls -1 ./node_modules/@types | xargs -I xx $(NPM) uninstall @types/xx
	find ./ -path "./node_modules/@types" | xargs -I xx rmdir xx
	ls -1 ./node_modules | xargs -I xx $(NPM) uninstall xx
	mkdir -p ./node_modules/.bin # same; we don't want rm/rmdir to fail
	find ./ -path "./node_modules/.bin/*" | xargs rm -f
	rmdir ./node_modules/.bin
	rmdir ./node_modules

install: $(LINK)
$(LINK):
	ln -s $(CURDIR)/bin/schtickle $(LINK)

# There is no file with the given name associated with these make targets; we
# give them names for our own reasons.
.PHONY: all clean squeakyclean rebuild install
